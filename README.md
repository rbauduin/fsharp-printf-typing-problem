This repo illustrates a problem I encountered with PrintfFormat handling functions with type parameters placed in different modules split in multiple files.

A function called `formatStringHandler` in module `Utils` handles the `PrintfFormat` argument. In this simplified code, it simply calls `sprintf`:

```
module Utils
    open System
    let formatStringHandler  (callback: string -> Type list-> obj list -> 'ReturnType)
                             (format:PrintfFormat<'PrintfType,_,_,'ReturnType>)
                             :'PrintfType =
        sprintf format
```

In the main file `Program.fs`, a function named `callFromSameFile<'a>` calls this `formatStringHandler` and passes it a callback as expected. `callFromSameFile<'a>` has
one type argument. Not used here, but important as that's what causing trouble in the end:

```
let callback (format:string) (types:System.Type list) (values:obj list) =
    "result"
let callFromSameFile<'a> format =
    Utils.formatStringHandler callback format
```

Calling that function works fine:
```
    let s1 = callFromSameFile<string> "insert into instances(entity_id) VALUES (%d,%d) RETURNING id" 4 5
```

However, when that same function is placed in another module (and renamed to `callFromOtherFile<'a>`, the similar call
from the main program:
```
    let s2 = DBA.callFromOtherFile<string> "insert into instances(entity_id) VALUES (%d,%d) RETURNING id" 4 5
```
fails with the error `error FS0003: This value is not a function and cannot be applied.`
