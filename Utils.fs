module Utils
    open System
    let formatStringHandler  (callback: string -> Type list-> obj list -> 'ReturnType)
                             (format:PrintfFormat<'PrintfType,_,_,'ReturnType>)
                             :'PrintfType =
        sprintf format
