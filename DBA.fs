module DBA
    open System.Text.RegularExpressions
    open System.Collections.Generic
    let callback (format:string) (types:System.Type list) (values:obj list) =
        "result"
    // the type argument is the culprit here. delete <'a> and it works fine
    let callFromOtherFile<'a> format =
        Utils.formatStringHandler callback format
