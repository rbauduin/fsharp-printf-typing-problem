﻿// Learn more about F# at http://fsharp.org

open System
open Microsoft.FSharp.Reflection
open System.Text.RegularExpressions
open System.Collections.Generic
open Microsoft.FSharp.Reflection

let callback (format:string) (types:System.Type list) (values:obj list) =
    "result"
let callFromSameFile<'a> format =
    Utils.formatStringHandler callback format

[<EntryPoint>]
let main argv =
    let s1 = callFromSameFile<string> "insert into instances(entity_id) VALUES (%d,%d) RETURNING id" 4 5
    printf "%A" s1
    // next line fails with
    // error FS0003: This value is not a function and cannot be applied.
    // the problem is the type argument of callFromOtherFile
    let s2 = DBA.callFromOtherFile<string> "insert into instances(entity_id) VALUES (%d,%d) RETURNING id" 4 5
    0 // return an integer exit code
